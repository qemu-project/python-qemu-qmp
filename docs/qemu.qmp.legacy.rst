Legacy API
==========

.. automodule:: qemu.qmp.legacy
   :members:
   :undoc-members:
   :show-inheritance:
